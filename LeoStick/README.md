# Project for LeoStick 

 Given the prototype of playground-project for LeoStick.
 
## Specification

|Spec Type | Spec Description |
|:------:|:-------------:|
|**MCU Type**|Atmel ATmega32u4|
|**Operating Voltage**| 5V |
|**MCU Clock Speed**| 16 MHz|
|**Input Voltage**|5V DC via USB port or "5V" header|
|**Digital I/O pins**| 14 (6 provide PWM output)|
|**PWM Output Pins**|3, 5, 6, 9, 10 and 11 (V2.0), 3, 5, 9, 10 and 11 (earlier models, V1.0)|
|**Analog Input Pins**|6 (analog input pins also support digital I/O,giving 20 digital I/O total if required)|
|**Analog Resolution**|10 bits, 0-1023 at 5V AREF is approx 0.00488V; 4.88mV per step|
|**Current Per I/O Pin**|40 mA maximum|
|**Total Current For All I/O Pins**|	200mA maximum|
|**Current For 3.3V Output**|50mA maximum|
|**Flash Memory**|32 KB Flash Memory,(~2 KB is used by the bootloader)|
|**SRAM, EEPROM**|2.5 KB SRAM, 1 KB EEPROM|
|**Serial**|1 x hardware USART (RX=D0, TX=D1)|
|**SPI**|On six-pin ICSP header (MISO=1, SCK=3, MOSI=4, see schematic for layout.)|
|**SPI II**|I2C (aka TWI, Two Wire Interface)|| 	
|**I2C aka TWI**|(SDA=D2, SCL=D3.)|
|**Other I**|Integrated USB programming and communication port.|
## Compatible with 
  - Arduino
  
## List of Prototype
  - Buzzer (from the board itself)
  
